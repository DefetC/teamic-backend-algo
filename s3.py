import localstack_client.session
import json

LOCAL_TEST_BUCKET = "local-bucket"

# Create localstack s3 session
session = localstack_client.session.Session()
s3 = session.client('s3')

# Output the bucket names
response = s3.list_buckets()
print('Existing buckets:')
for bucket in response['Buckets']:
    print(f'  {bucket["Name"]}')

# Create the bucket
if LOCAL_TEST_BUCKET not in s3.list_buckets()['Buckets']:
    s3.create_bucket(Bucket=LOCAL_TEST_BUCKET)

# Set its policy
bucket_policy = {
    'Version': '2019-08-24',
    'Statement': [{
        'Sid': 'AddPerm',
        'Effect': 'Allow',
        'Principal': '*',
        'Action': [
            's3:GetObject',
            's3:DeleteObject',
            's3:PutObject',

            's3:GetObjectAcl',
            's3:PutObjectAcl',

            's3:GetObjectVersionAcl', # GET ACL (for a Specific Version of the Object)
        ],
        'Resource': f'arn:aws:s3:::{LOCAL_TEST_BUCKET}/*'
    }]
}
# Convert the policy from JSON dict to string
bucket_policy = json.dumps(bucket_policy)

s3.put_bucket_policy(Bucket=LOCAL_TEST_BUCKET, Policy=bucket_policy)

# Get bucket policy
# result = s3.get_bucket_policy(Bucket='local-bucket')
# print(result['Policy'])

# # Upload a file
# with open("inactiveenv.txt", "rb") as f:
#     s3.upload_fileobj(f, "local-bucket", "testfile-inactiveenv.txt")

# # Download a file
# with open('downloaded_inactiveenv.txt', 'wb') as f:
#     s3.download_fileobj('local-bucket', 'testfile-inactiveenv.txt', f)

