'''
Name: app/utils.py
Author: David Stern - Team IC.
Purpose: Adds a converter for URL paths, to be used in blueprint routes.
'''

# Converters
from werkzeug.routing import BaseConverter

class ListConverter(BaseConverter):
    '''
    Automatically converts lists of file names (cannot include `;` in these
    file names) to a list of file names, when used as a URL converter.
    
    Registered in app in app/__init__.py via: 
        `app.url_map.converters['file_list'] = ListConverter`

    This allows us to use @bp.route('/path/<file_list:files>', methods=[...]),
    and file_list will be converted from, for example: file1.txt;file2.pdf
        -> ["file1.txt", "file2.pdf"]

    E.g. http://...com/api/submissions/file1.txt;file2.pdf?=submission_id=...
    '''

    def to_python(self, value):
        return value.split(';')

    def to_url(self, values):
        return ';'.join(BaseConverter.to_url(value)
                        for value in values)


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print(f"{method.__name__}  {(te-ts)*1000:2.2f} ")
            # print ('%r  %2.2f ms' % \
            #       (method.__name__, (te - ts) * 1000))
        return result
    return timed
