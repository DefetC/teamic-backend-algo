'''
Name: app/__init__.py
Author: David Stern - Team IC
Purpose: Initialises the Flask Application's Database & defines the 
         configuration process of the application.
Re-use: Re-uses (in part) setup of the create_app factory from
        github.com/miguelgrinberg/microblog
'''
from flask import Flask
from config_app import config
from app.models import Submissions
from app.utils import ListConverter

# Redis Task Queue
from redis import Redis
import rq

def create_app(config_name):
    '''Application Factory for our Flask Application.
    '''
    print(f"Starting app with ")

    # Initialise Flask app w/ Config
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    # Redis & Redis Queue for Background jobs
    # Local - (1) Need to have redis server running: `redis-server`, and 
    #         (2) Run a rq worker: rq worker ta-api-tasks
    # @TODO: Add AWS Deployment Instructions.
    # E.g. For Heroku, ensure worker is running: 
    #               heroku ps:scale worker=1 -a tp-data-api
    if 'REDIS_URL' in app.config:
        redis_url = app.config['REDIS_URL']
        print(f"USING REDIS with URL: {redis_url}")
        app.redis = Redis.from_url(redis_url)
        app.task_queue = rq.Queue('ic-api-tasks', connection=app.redis)

    # Add SSL redirections, to return HTTPS message, via Heroku
    if app.config['SSL_REDIRECT']:
        from flask_sslify import SSLify
        sslify = SSLify(app)

    # Add Custom URL Converter
    app.url_map.converters['list'] = ListConverter

    # Blueprints for various components
    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    return app

from app import models
