'''
Name: app/api/response_tools.py
Author: David Stern - Team IC.
Purpose: Provides tools for handling general API requests.
Re-Use: Functions adapted from Miguel Grinberg's Flask Tutorial Code 
        (github.com/miguelgrinberg/microblog)
'''

# Dictionary of HTTP Status Codes
from werkzeug.http import HTTP_STATUS_CODES
from werkzeug.datastructures import FileStorage
from flask import jsonify
import json

ALLOWED_MIMETYPES = set(["application/octet-stream"])


def allowed_filetype(file: FileStorage) -> bool:
    '''Ensures only allowed filetypes are allowed to be stored in the database.
    '''
    mimetype = getattr(file, "mimetype", None)
    return mimetype is not None and mimetype in ALLOWED_MIMETYPES


def html_response(status_code, message=None, error_msg=False, loc=None,
                  deleted_obj=None, data=None):
    '''Creates and returns HTML responses given a HTML status code & 
       optionally, a message, whether the response is an error response, and a
       location.
    '''
    payload = {}
    if message:
        payload['message'] = message
    if error_msg:
        payload['error'] = HTTP_STATUS_CODES.get(status_code, 'Unknown error')
    if deleted_obj:
        payload['deleted'] = deleted_obj
    if data:
        payload['data'] = data
    response = jsonify(payload)
    response.status_code = status_code
    if loc:
        response.headers['location'] = loc
    return response

