'''
Name: app/api/__init__.py
Author: David Stern - Team IC
Purpose: Creates the api package and its blueprint.

Blueprint ensures that all api requests are via the /api/ sub-directory.
'''
from flask import Blueprint

bp = Blueprint('api', __name__)

from app.api import submissions
