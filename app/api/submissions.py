'''
Name: app/api/submissions.py
Purpose: Internal API that receives data and initialises processing, as well as
         responding to requests for authorship analysis results.
Author: David Stern - Team IC

TODO: ...
'''

# Flask API
from flask import jsonify, request, url_for, current_app
import json

# Debug Messages
from pprint import PrettyPrinter

# Our app
from app import create_app
from app.models import Submissions, FileMap
from app.api import bp
from app.algo.processor import process              # Process to add to queue
from app.api.response_tools import html_response    # , allowed_filetype

REQUIRED_FIELDS = ["submission_id", "student_id", "workshop_id", 
                   "assignment_id", "subject_id"]


# -------------------------------------------------------------------------- #
#                           Submissions API endpoints
# -------------------------------------------------------------------------- #

@bp.route('/submissions/<list:files>', methods=['POST'])
def create_submission(files):
    '''
    Receives a submission's id, its student's id, and the associated name of 
    the submission's file in the S3 bucket, creates a Submission in its DB,
    and enqueues in a REDIS queue (1) Document retrieval, which then queues 
    (2) Authorship detection on that document's text. 

    After enqueueing, it responds to the POST request.

    Performs verification of uniqueness of submission id, and ensures all
    required fields are sent in the request before processing.

    Example POST request using httpie (command-line tool installed via pipenv,
    that is called with `http`):
    http POST "http://127.0.0.1:8000/api/submissions/known01.txt;known02.txt" 
    submission_id=smn1 student_id=st1 subject_id=sjt1 workshop_id=ws1 
    assignment_id=ass1
        ^ JSON data processed by httpie into a json payload.
    '''
    data = request.get_json() or {}

    # 400 Response if request doesn't include all required fields
    for field in REQUIRED_FIELDS:
        if field not in data:
            msg = f'Must include the {field} field.'
            return html_response(400, error_msg=True, message=msg)

    # 400 response if request doesn't include a single file
    if len(files) < 1:
        return html_response(400, error_msg=True, 
                             message=f'Must include at least one file.')

    # 400 response if submission with same submission_id already exists
    sub_id = data['submission_id']
    if Submissions.count(hash_key=sub_id) > 0:
        msg = f'Invalid Request. Duplicate submission_id {sub_id}.'
        return html_response(400, error_msg=True, message=msg)
    # Check all files have valid file types
    for fname in files:
        if fname[-4:] not in [".txt", ".pdf"]:
            msg = f'Invalid Request. File {fname} is not a supported file type'
            return html_response(400, error_msg=True, 
                                 message=msg + ' (.pdf, .txt).')

    # Ensure file names are unique
    #  ^(important for the `file_texts` dict in the `process_file` function.)
    if len(files) != len(set(files)):
        msg = 'Invalid request; duplicate filenames given.'
        return html_response(400, error_msg=True, message=msg)

    # Create the resource & save it in the DB
    submission = Submissions(
                    submission_id=sub_id,                   # Hash Key
                    student_id=data['student_id'],          # Range Key
                    files=[FileMap(filename=fn) for fn in files],
                    
                    # Fields to aid queries
                    subject_id=data['subject_id'],
                    workshop_id=data['workshop_id'],
                    assignment_id=data['assignment_id'],
                    )
    submission.save()

    # Queue S3 .txt document retrieval & file processing
    # (Timeout = 9600s = 2h40mins)
    if 'REDIS_URL' in current_app.config:
        current_app.task_queue.enqueue(process, sub_id)


    # Return formatted response
    response = jsonify(submission.to_dict())
    response.status_code = 202        # Success, but processing is incomplete
    response.headers['Location'] = url_for('api.get_submission',
                                           submission_id=sub_id)
    return response

@bp.route('/submissions/single', methods=['GET'])
def get_submission():
    '''
    GET request for single submission, given its submission id.
    Example GET Request using httpie:
        http GET 'http://127.0.0.1:8000/api/submissions/single?submission_id=1d'
    '''

    # Check submission_id is given and that the submission exists
    data = request.get_json() or {}
    submission_id = get_arg(data, 'submission_id')
    if submission_id is None or Submissions.count(hash_key=submission_id) < 1:
        return submission_id_error(submission_id)

    # Alter response if complete 
    complete = True if "true" == get_arg(data, 'complete').lower() else False
    return jsonify(
            Submissions.get_submission(submission_id).to_dict(complete))


@bp.route('/submissions/many/<list:submission_ids>', methods=['GET'])
def get_submissions(submission_ids):
    '''
    GET request for multiple submission, given the submissions' ids.
    Example GET request using httpie; assumes submissions w/ ids `smn1` & `smn2`
    are in the database:
        http GET "http://127.0.0.1:8000/api/submissions/many/smn2;smn1"
    '''

    # Detect invalid submissions
    if len(submission_ids) != len(set(submission_ids)):
        msg = "Invalid Request. Tried to retrieve non-unique submissions."
        return html_response(400, error_msg=True, message=msg)

    results = []
    for sub_id in submission_ids:

        # Check the id is valid
        if sub_id is None or Submissions.count(hash_key=sub_id) < 1:
            return submission_id_error(sub_id)

        # Get the submission
        results.append(Submissions.get_submission(sub_id).to_dict())

    msg = f"Retrieved all specified submissions"
    return html_response(200, message=msg, data=results)


@bp.route('/submissions/subject', methods=['GET'])
def get_submission_by_subject():
    '''
    GET request for all submissions of the given subject.
    Example GET request using httpie; assumes submissions w/ subject `sct1` are
    in the database:
        http GET "http://127.0.0.1:8000/api/submissions/subject?subject_id=sct1"
    '''
    data = request.get_json() or {}
    subject_id = get_arg(data, 'subject_id')
    msg = f"Retrieved all submissions in specified subject"
    results = Submissions.get_submissions_by_subject(subject_id)
    return html_response(200, message=msg, data=results)

@bp.route('/submissions/workshop', methods=['GET'])
def get_submission_by_workshop():
    '''
    GET request for all submissions of the given workshop.
    Example GET request using httpie; assumes submissions w/ workshop `ws1` are
    in the database:
      http GET "http://127.0.0.1:8000/api/submissions/workshop?workshop_id=ws1"
    '''
    data = request.get_json() or {}
    workshop_id = get_arg(data, 'workshop_id')
    msg = f"Retrieved all submissions in specified workshop"
    results = Submissions.get_submissions_by_workshop(workshop_id)
    return html_response(200, message=msg, data=results)


@bp.route('/submissions/assignment', methods=['GET'])
def get_submission_by_assignment():
    '''
    GET request for all submissions of the given assignment.
    Example GET request using httpie; assumes submissions w/ assignment `ass1` 
    are in the database:
    http GET 
        "http://127.0.0.1:8000/api/submissions/assignment?assignment_id=ass1"
    '''
    data = request.get_json() or {}
    assignment_id = get_arg(data, 'assignment_id')
    msg = f"Retrieved all submissions in specified assignment"
    results = Submissions.get_submissions_by_assignment(assignment_id)
    return html_response(200, message=msg, data=results)


@bp.route('/submissions/delete', methods=['PUT'])
def delete_submission():
    '''
    PUT request to delete a submission, using its submission_id.
    Example PUT request using httpie:
    http PUT 'http://127.0.0.1:8000/api/submissions/delete?submission_id=su1'
    '''

    # Check submission_id is given and that the submission exists
    data = request.get_json() or {}
    submission_id = get_arg(data, 'submission_id')
    if submission_id is None or Submissions.count(hash_key=submission_id) < 1:
        return submission_id_error(submission_id)

    # Retrieve and delete the submission
    submission = Submissions.get_submission(submission_id)
    submission.delete()

    # Return a response with the submission
    deleted_msg = f"Deleted submission {submission_id}"
    return html_response(200, deleted_msg, deleted_obj=submission.to_dict())


@bp.route('/submissions/validate', methods=['PUT'])
def validate_submission():
    '''
    PUT request to update submissions as valid or invalid.
    Runnable Examples (assumed a submission with submission id `smn1` already 
    exists:
        http POST "http://127.0.0.1:8000/api/submissions/validate?submission_id=smn1&valid=true"
        http POST "http://127.0.0.1:8000/api/submissions/validate?submission_id=smn1&valid=false"
    '''
    # Check submission_id is given and that the submission exists
    data = request.get_json() or {}
    submission_id = get_arg(data, 'submission_id')
    if submission_id is None or Submissions.count(hash_key=submission_id) < 1:
        return submission_id_error(submission_id)

    # Check valid argument is given & correct
    valid = get_arg(data, 'valid').lower()
    if valid == '':
        return html_response(400, 'Must include valid argument.')
    valid = True if valid == 'true' else False

    # Update Database
    submission = Submissions.get_submission(submission_id)
    submission.update(actions=[Submissions.validated.set(valid)])

    # Return updated object
    return jsonify(submission.to_dict())


def submission_id_error(submission_id):
    '''
    Return appropriate error given the issue with the submission_id.
    Assumes that the two conditions have already been checked, they are checked
    here merely to differentiate the messages & codes sent.
    '''

    # Check submission_id argument given
    if submission_id is None:
        return html_response(400, 'Must include submission_id argument.')

    # Check submission with that id exists. If the above if expression is not 
    # true, then `Submissions.count(hash_key=submission_id) < 1` must have been.
    else:
        return html_response(404, 'No submission found with ' + 
                                  f'submission_id {submission_id}')

def get_arg(data, arg):
    return data[arg] if arg in data else (
        '' if arg not in request.args else request.args.get(arg))
