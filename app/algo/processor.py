'''
Name: app/algo/processor.py
Author: David Stern - Team IC.
Purpose: Manages File download and processing.
'''


# Amazon S3 Downloading
import boto3
from boto3.session import Session
from botocore.exceptions import ClientError

# File Processing
import io
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage
from langid.langid import LanguageIdentifier, model

# Algorithm
from sklearn.feature_extraction.text import CountVectorizer
from app.algo.metrics import metrics
import numpy as np

# Database Interaction
from app.models import Submissions, MetricMap, FileMap, WordMap
from app import create_app

# Settings
from flask import current_app
import os

from settings import REQUIRED_SETTINGS


def process(submission_id):
    '''
    The task queued in the REDIS queue and processed by independent worker
    processes. The independence of these workers is why the application has to
    be created for them and the function must be executed in the application's
    context.
    '''
    # Log that the session is being processed
    print("Executing queued task...")

    # Get an application context to run the process, then run it
    app = create_app(os.getenv('FLASK_CONFIG') or 'default')
    with app.app_context():
        process_file(submission_id)
    print("Completed processing task successfully!")


def process_file(submission_id):
    '''
    Downloads a pdf file from S3, converts to a string, calculates the metric
    and updates the database entry.
    '''
    # Retrieve the named submission
    submission = Submissions.get_submission(submission_id)
    if submission is None:
        print(f"No such submission ({submission_id}) found in the Database")
        return

    # 1. Download the files from S3
    doc_bytes_objs = file_downloader(submission.files)

    # 2. Convert to string and add to DB
    file_texts = {}
    for fname, fbytes in doc_bytes_objs:
        if fname[-4:] == ".pdf":
            file_texts[fname] = convert_pdf(fbytes)
        elif fname[-4:] == ".txt":
            file_texts[fname] = fbytes.read().decode()

        # Otherwise, raise an error, we do not handle this file type
        # Should never reach this because of check in create_submission
        else:
            raise Exception('Invalid File Type', f'file name = {fname}')

    # 3. Get all of the students' previous files as a list of strings.
    history = []
    for sub in Submissions.get_validated_subs_by_student(submission.student_id):
        for file in sub.files:
            history.append(file.text)

    # 4. Load in tokenisation settings, asserting they are all there
    settings = current_app.config['ALGO_SETTINGS']['tokenisation']
    for setting in REQUIRED_SETTINGS:
        if setting not in settings:
            print(settings)
        assert setting in settings, f"Missing {setting} from settings"
    min_n, max_n = settings["min_n"], settings["max_n"]
    max_feats, ngram_type = settings["max_feats"], settings["ngram_type"]
    assert min_n <= max_n, "Min ngram size is larger than the maximum size"

    # 5. Calculate Metrics and store the files & metrics in the database
    new_files = []
    lang_IDer = LanguageIdentifier.from_modelstring(model, norm_probs=True)
    for fname, file_text in file_texts.items():

        # Set metrics to 1.0 if this is the first submission
        if not history:
            print("Null history. Settings metrics to 0.")     # Logging
        else:
            word_matrix, unknown_vector, feat_names = (
                get_ngrams(history, [file_text], min_n, max_n, max_feats, 
                           ngram_type))
        # Calculate all metrics and corresponding MetricMaps
        metric_list = []
        for mname, metric_func in metrics.items():
            if not history:
                metric = 1.0
            else:
                metric = metric_func(word_matrix, unknown_vector) # [0][0]
                print(type(metric), metric, mname)
            metric_list.append(MetricMap(metric=metric, metric_name=mname, 
                                         metric_history=len(history)))


        # calculate sentences
        sentences = []
        if not history:
            sentences = []
        else:
            feature_sentences, history_values, new_values\
                = get_sentence(word_matrix, unknown_vector, feat_names)
            for i in range(len(feature_sentences)):
                sentences.append(WordMap(word=feature_sentences[i], new_file_value=int(history_values[i]), old_file_value=int(new_values[i])))


        # Create new completed FileMap
        new_files.append(FileMap(filename=fname, text=file_text, 
                                 language=lang_IDer.classify(file_text)[0],
                                 metrics=metric_list, sentences=sentences))

    # Update DB (doesn't have to re-create the entry, unlike .save())
    submission.update(actions=[Submissions.files.set(new_files)])


def file_downloader(filemaps):
    '''
    Given files named in a filemap, download each file's bytes from an Amazon S3 
    bucket.
    '''
    # 1. Connect to the S3 Bucket
    session = Session()
    s3_client = session.client('s3')

    # 2. Get filename, file contents (bytes) tuple pairs
    file_byte_objects = []
    bucket_name = os.getenv('AWS_S3_BUCKET')
    for fmap in filemaps:
        fname = fmap.filename
        try:
            file_byte_objects.append((
                fname,
                io.BytesIO(s3_client.get_object(Bucket=bucket_name,
                                                Key=fname)['Body'].read())
                ))
        except (KeyError, ClientError) as e:
            print(f"Failed retrieval of {fname} from {bucket_name}")

            # If file not found in S3, log the list of keys
            if e.response['Error']['Code'] == 'NoSuchKey':
                all_keys = [k for k in 
                            get_matching_s3_keys(s3_client, bucket_name)]
                print(f"Keys in bucket = {all_keys}")

            raise e

    return file_byte_objects


def get_matching_s3_keys(s3, bucket, prefix='', suffix=''):
    """
    Source: https://alexwlchan.net/2017/07/listing-s3-keys/

    Generate the keys in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch keys that start with this prefix (optional).
    :param suffix: Only fetch keys that end with this suffix (optional).
    """
    kwargs = {'Bucket': bucket}

    # If the prefix is a single string (not a tuple of strings), we can
    # do the filtering directly in the S3 API.
    if isinstance(prefix, str):
        kwargs['Prefix'] = prefix

    while True:

        # The S3 API response is a large blob of metadata.
        # 'Contents' contains information about the listed objects.
        resp = s3.list_objects_v2(**kwargs)
        for obj in resp['Contents']:
            key = obj['Key']
            if key.startswith(prefix) and key.endswith(suffix):
                yield key

        # The S3 API is paginated, returning up to 1000 keys at a time.
        # Pass the continuation token into the next response, until we
        # reach the final page (when this field is missing).
        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break



def convert_pdf(pdf_bytes):
    '''
    Source: http://www.blog.pythonlibrary.org/2018/05/03/
            exporting-data-from-pdfs-with-python/
    '''
    # Stores the converter's results
    fake_file_handle = io.StringIO()

    # Used to manage the conversion & interpretation process
    resource_manager = PDFResourceManager()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)
 
    # Process each page's bytes into the fake_file_handle
    for page in PDFPage.get_pages(pdf_bytes, 
                                  caching=True,
                                  check_extractable=True):
        page_interpreter.process_page(page)

    # Retrieve the text from the file
    text = fake_file_handle.getvalue()
 
    # close open handles
    converter.close()
    fake_file_handle.close()
    
    return text if text else None

def get_ngrams(history, new_doc, min_n, max_n, max_feats, ngram_type):
    '''
    history (list(str)): A list with strings representing past documents whose
                         authorship is known.
    new_doc (list(str)): A list with a single string representing the document
                         content being analysed.
    min_n (int): The minimum ngram size.
    max_n (int): The maximum ngram size.
    max_feats (int): The maximum number of features the ngram arrays will have.
    ngram_type (str): Specifies the type of ngram analyser used (character,
                      characters with word boundaries, words, etc.)
    '''

    # Create a vector of [doc1: {word1_count, word2_count}, doc2: {...}, ...]
    vectoriser = CountVectorizer(ngram_range=(min_n, max_n),
                                 analyzer=ngram_type, max_features=max_feats)
    # vectoriser = CountVectorizer(ngram_range=(min_n, max_n),max_features=max_feats)


    # Get the top n-grams & get the n-gram count in the new document
    word_matrix = np.sum(vectoriser.fit_transform(history).toarray(),
                         axis=0).reshape(1, -1)
    unknown_vector = vectoriser.transform(new_doc).toarray().reshape(1, -1)
    
    # Return the ngram counts of the history and the new document to be assessed
    return (word_matrix, unknown_vector, vectoriser.get_feature_names())

def get_sentence(word_matrix, unknown_vector, feature_names):
    # history_indexes = np.argpartition(np.squeeze(word_matrix), -5)[-5:]
    # history_feature_sentences = [feature_names[i] for i in list(history_indexes)]
    # history_values = [word_matrix[0][i] for i in list(history_indexes)]
    # new_indexes = np.argpartition(np.squeeze(unknown_vector), -5)[-5:]
    # new_feature_sentences = [feature_names[i] for i in list(new_indexes)]
    # new_values = [unknown_vector[0][i] for i in list(new_indexes)]
    # return history_feature_sentences, history_values, new_feature_sentences, new_values

    new_values_average = np.squeeze(unknown_vector) / np.sum(np.squeeze(unknown_vector))
    history_average = np.squeeze(word_matrix)/np.sum(np.squeeze(word_matrix))
    divide_values = np.divide(new_values_average, history_average)
    divide_indexes = np.argpartition(divide_values, -5)[-5:]
    features = [feature_names[i] for i in list(divide_indexes)]
    history_values = [word_matrix[0][i] for i in list(divide_indexes)]
    new_values = [unknown_vector[0][i] for i in list(divide_indexes)]
    return features, history_values, new_values





'''
This works having:
1. `known01.txt` uploaded on the relevant S3 bucket; and
2. After running the app and sending it the following POST request, which 
   creates the `su1` submission:
     http POST http://127.0.0.1:8000/api/submissions/known.txt; 
     submission_id=su1 student_id=st1 subject_id=sj1 workshop_id=ws1 
     assignment_id=as1

Run this as a script via: `python -m app.algo.processor`
'''
if __name__ == "__main__":
    # eg_fname = 'known01.txt'
    eg_submission_id = "su1"
    process_file(eg_submission_id)
