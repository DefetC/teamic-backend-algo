'''
Name: app/algo/algo_script.py
Author: David Stern - Team IC.
Purpose: Tests metrics on PAN13 examples, calculating thresholds and accuracy
achieved given those thresholds.

Use: With the virtual env active, run `python -m app.algo.algo_script` to 
'''

import os
import distance
import glob
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd
import numpy as np
from collections import defaultdict as dd
from app.algo.metrics import metrics
from langid.langid import LanguageIdentifier, model
from tqdm import tqdm
import difflib
import re

def pan13_test():
    # Get the similarity scores for each user
    user_dict = calc_stats(metrics, nmin=4, nmax=4, max_feats=1000, 
                           ngram_type="char_wb")

    # Get the truth data & add it to the user_dict
    # Note: This file must be run from the main repo folder via the command:
    #           `python -m app.algo.algo_script`
    truth_data = pd.read_csv("./pan13_test/truth.txt", sep=" ", header=None,
                             # We will name the value column 'val'
                             names=['val'], 
                             # The first col for each row = user/folder names
                             index_col=0,
                             # Convert column 1 to 1 if "Y" else 0.
                             converters={1: lambda x: 1 if x == "Y" else 0})

    # Select & store truth values from truth_data
    for user in user_dict:
        user_dict[user]["truth"] = truth_data.loc[user, 'val'] 
                                                # ^By row, ^then by col
    # Very preliminary analysis of the metrics
    labeled_results = {}      # Useful for accesing by user, can be used later
    tfs = {name: ([], []) for name in metrics.keys()}
    thresholds = {name: -1 for name in metrics.keys()}
    for user in user_dict:
        labeled_results[user] = {}
        labeled_results[user]['truth'] = user_dict[user]['truth']
        for metric_name in metrics:
            labeled_results[user][metric_name] = user_dict[user][metric_name]
        tf_list = 0 if user_dict[user]['truth'] else 1
        for metric_name in metrics.keys():
            tfs[metric_name][tf_list].append(labeled_results[user][metric_name])

    # Print each group's mean, min and max
    print(f"{65*'-'}\n| Metric\t\t| Class\t\t| Min\t| Mean\t| Max\t|\n{65*'-'}")
    for metric_name, (trues, falses) in tfs.items():
        for (vals, name) in ((trues, "Trues"), (falses, "Falses")):
            print(f"| {metric_name:<10}\t| {name:<6}:\t| {np.amin(vals):.2}\t", 
                  f"| {np.mean(vals):.2}\t| {np.amax(vals):.2}\t|")

        # Threshold Calculation - simply the mean value of the metric.
        thresholds[metric_name] = (np.mean(trues + falses))
    print(65*'-')

    # Threshold result
    # This threshold is used to judge whether a method judges a document as 
    # written by the author of the set of documents in the history, or not.
    # We do not take responsibility for this method or its accuracy.
    print("\nThreshold Calculations")
    for metric_name, threshold in thresholds.items():
        trues, falses = tfs[metric_name]
        total = len(trues + falses)
        correct_trues = (trues >= threshold).sum()
        correct_falses = (falses < threshold).sum()
        incorrect_trues = (trues < threshold).sum()
        incorrect_falses = (falses >= threshold).sum()
        accuracy = (correct_trues + correct_falses) / len(trues + falses)
        print(f"{metric_name:<10}:\t With threshold={threshold:.4},", 
              f"accuracy={accuracy:.4}")


def calc_stats(metrics, nmin, nmax, max_feats, ngram_type):
    '''
    metrics (dict): Mapping from metric names to the functions.
                    E.g. {"cosine": cosine_similarity, ...}
    nmin
    nmax
    max_feats
    ngram_type
    '''

    # Create a dictionary containing users and their histories and new docs.
    # 
    user_dict = get_content_from_docs()
    for user in tqdm(user_dict, ascii=True, desc="Calculating metrics"):

        # Access user's history (list of strings) & new file ([str] of len 1)
        contents = user_dict[user]["history"]
        unknown_content = user_dict[user]["unknownfile"]
        # word_matrix is the result of vectorized history contents
        # unknown_doc_vector is the result of vectorized unknown contents
        word_matrix, unknown_doc_vector = get_ngrams(contents, unknown_content,
                                                     nmin, nmax, max_feats, 
                                                     ngram_type)
        # Calc & store metrics (e.g. cosine distance)
        for metric_name, metric in metrics.items():
            user_dict[user][metric_name] = metric(word_matrix, 
                                                  unknown_doc_vector)

    # Detect language of each new unknown file
    # for user in tqdm(user_dict, ascii=True, desc="Detecting languages"):
    #     user_dict[user]["language"] = get_lang(
    #         ' '.join(user_dict[user]["unknownfile"]))
    return user_dict
    

def get_lang(text_string):
    return LanguageIdentifier.from_modelstring(model, 
               norm_probs=True).classify(text_string)[0]


def txt_to_str(fname):
    with open(fname, mode="r", encoding="utf-8") as f:
        return "".join([preprocess(x) for x in f.readlines()])

# preprocess the sentence:
# 1. lower case
# 2. strip
def preprocess(sentence):
    return sentence.strip().lower()

def get_content_from_docs():
    '''
    Return a mapping of user (folder) names to each user's history, new files,
    etc.
    '''
    user_dict = dd(dict)
    for user_folder in glob.glob(r"./pan13_test/*"):

        if '.txt' in user_folder:
            continue

        # Initialise user dict entry
        user = user_folder.split("/")[-1]
        user_dict[user]["history"] = []

        # Add to user's history and the unknown file
        for file in glob.glob(os.path.join(user_folder, 'known*.txt')):
            user_dict[user]["history"].append(txt_to_str(file))

        for unknownfile in glob.glob(os.path.join(user_folder, 'unknown.txt')):
            user_dict[user]["unknownfile"] = [txt_to_str(unknownfile)]
    return user_dict


def get_ngrams(history, new_doc, min_n, max_n, max_feats, analyzer):
    '''
    history (list(str)): A list with strings representing past documents whose
                         authorship is known.
    new_doc (list(str)): A list with a single string representing the document
                         content being analysed.
    min_n: 
    max_n: 
    max_feats: 
    analyzer:  
    '''
    # Create a vector of [doc1: {word1_count, word2_count}, doc2: {...}, ...]
    vectoriser = CountVectorizer(ngram_range=(min_n, max_n), 
                                 max_features=max_feats)

                    

    # Get the top n-grams & get the n-gram count in the new document
    result = vectoriser.fit_transform(history).toarray()
    # word_matrix = np.sum(result, axis=0)
    word_matrix = result
    debug = False
    if debug:
        print(f"Result (dimensions, shape): ({result.ndim}, {result.shape})")
        print(f"word_matrix (dimns, shape): ({word_matrix.ndim}, {word_matrix.shape})")
        print("result: ", result)
        print("word_matrix: ", word_matrix)
    # word_matrix = np.sum(vectoriser.fit_transform(history).toarray(),
    #                      axis=0)
    unknown_vector = vectoriser.transform(new_doc).toarray()
    print(vectoriser.get_feature_names())     

    # # Reshape the data using X.reshape(1, -1); treat it as a single sample
    # word_matrix = word_matrix.reshape(1, -1)
    if debug:
        print("\nAfter reshape(1, -1):")
        print(f"word_matrix (dimns, shape): ({word_matrix.ndim}, {word_matrix.shape})")
        print("word_matrix: ", word_matrix)
    unknown_vector = unknown_vector.reshape(1, -1)

    if debug:
        error = 1 + "e"
    
    # Return the ngram counts of the history and the new document to be assessed
    return word_matrix, unknown_vector

def partition_sentence(article):
    content_list = article.split('. ')
    output_list = []
    for i in range(len(content_list)):
        output_list.append(re.split(r' *[\.\?!][\'"\)\]]* *', content_list[i]))
    return output_list
    
<<<<<<< HEAD
def global_edit_distance(history_content_list,new_content_list):
=======
def global_edit_distance(content_dic, new_content_dic):
>>>>>>> f24811fc46334e31b929bf056ad040087eb8ddff
    similarity_dic = {}
    for i in range(len(history_content_list)):
        for j in range(len(new_content_list)):
            # use tuple (i,j) as the key of the dict. note: i is the index of content_list, j is the inde of  new_content_list
            # reverse the similarity, so that the higher value means more similar 
            similarity_dic[(i,j)] = 1/ (ld(history_content_list[i], new_content_list[j]) +1)
    return similarity_dic

def string_similar(history_content_list, new_content_list):
    similarity_dic = {}
    for i in range(len(history_content_list)):
        for j in range(len(new_content_list)):
            # use tuple (i,j) as the key of the dict. note: i is the index of content_list, j is the inde of  new_content_list
            similarity_dic[(i,j)] = difflib.SequenceMatcher(None, ' '.join(history_content_list[i]), ' '.join(new_content_list[j])).quick_ratio()
    return similarity_dic 

def ld(sentence1,sentence2):
    m,n = len(sentence1)+1,len(sentence2)+1
    # initialize matrix
    matrix = [[0]*n for i in range(m)]
    matrix[0][0] = 0
    for i in range(1,m):
        matrix[i][0] = matrix[i-1][0] + 1
    for j in range(1,n):
        matrix[0][j] = matrix[0][j-1]+1  
    for i in range(1,m):
        for j in range(1,n):
            if sentence1[i-1] == sentence2[j-1]:
                matrix[i][j] = matrix[i-1][j-1]
            else:
                matrix[i][j] = min(matrix[i-1][j-1],matrix[i-1][j],matrix[i][j-1])+1
    return matrix[m-1][j-1]

def top_similarity(similarity_dic):
    descend = sorted(similarity_dic.items(), key=lambda item:item[1],reverse=True)
    top_five_similarity = descend[:5]
    return top_five_similarity


def calc_similarity():
    user_dict = get_content_from_docs()
    # print(user_dict['pan13_test\\EN01'])
    history_contents_list = user_dict['pan13_test\\EN01']["history"]
    new_contents_list = user_dict['pan13_test\\EN01']["unknownfile"]
    history_sencence_list = [] #  list of sentences of all past articles 
    new_sencence_list = [] #  list of sentences of all current articles
    for i in range(len(history_contents_list)):
        temp_list = partition_sentence(history_contents_list[i])
        history_sencence_list = history_sencence_list + temp_list

    for i in range(len(new_contents_list)):
        temp_list = partition_sentence(new_contents_list[i])
        new_sencence_list = new_sencence_list + temp_list
    # algorithm1: use Levenshtein distance
    similarity_dic = global_edit_distance(history_sencence_list, new_sencence_list)
    # algorithm2: use similarity_dic distance
    # similarity_dic = string_similar(history_sencence_list, new_sencence_list)
    similarities = top_similarity(similarity_dic)
    for i in range(len(similarities)):
        current_item = similarities[i]
        print('[top', i+1, '] score is:', current_item[1])
        print('[current sentence]',(' ').join(new_sencence_list[current_item[0][1]]))
        print('[old sentence]',(' ').join(history_sencence_list[current_item[0][0]]))
        print('-----------------------------------')


if __name__ == "__main__":
    # pan13_test()
    calc_similarity()
