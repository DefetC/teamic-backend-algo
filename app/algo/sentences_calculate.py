

def partition_sentence(contents, new_contents):
    content_dic = {}
    new_content_dic = {}
    content_list = []
    new_content_list = []
    for content in contents:
        content_list += content.split('.')
    for new_content in new_contents:
        new_content_list += new_content.split('.')
    for i in range(len(content_list)):
        content_dic['sentence' + str(i)] = content_list[i]
    for j in range(len(new_content_list)):
        new_content_dic['sentence' + str(j)] = new_content_list[j]
    return content_dic, new_content_dic


def global_edit_distance(content_dic, new_content_dic):
    similarity_dic = {}
    for i in content_dic.keys():
        for j in new_content_dic.keys():
            similarity_dic[(str(i), str(j))] = ld(content_dic[str(i)].split(' '),
                                                                new_content_dic[str(j)].split(' '))
    return similarity_dic


def ld(sentence1, sentence2):
    m, n = len(sentence1) + 1, len(sentence2) + 1
    # initialize matrix
    matrix = [[0] * n for i in range(m)]
    matrix[0][0] = 0
    for i in range(1, m):
        matrix[i][0] = matrix[i - 1][0] + 1
    for j in range(1, n):
        matrix[0][j] = matrix[0][j - 1] + 1
    for i in range(1, m):
        for j in range(1, n):
            if sentence1[i - 1] == sentence2[j - 1]:
                matrix[i][j] = matrix[i - 1][j - 1]
            else:
                matrix[i][j] = min(matrix[i - 1][j - 1], matrix[i - 1][j], matrix[i][j - 1]) + 1
    return matrix[m - 1][j - 1]


def top_similarity(similarity_dic):
    descend = sorted(similarity_dic.items(), key=lambda item: item[1], reverse=False)
    top_five_similarity = descend[:5]
    return top_five_similarity

def sentence_generate(contents, new_contents):
    contents_dic, new_contents_dic = partition_sentence(contents, new_contents)
    similarity_dic = global_edit_distance(contents_dic, new_contents_dic)
    top_five_similarity = top_similarity(similarity_dic)
    print(top_five_similarity)
    sentences_pairs = []
    for (index1, index2), _ in top_five_similarity:
        sentences_pairs.append((contents_dic[str(index1)], new_contents_dic[str(index2)]))
    print(sentences_pairs)
    return sentences_pairs

