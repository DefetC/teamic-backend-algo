'''
Name: app/models.py
Author: David Stern - Team Integrity-Checking
Purpose: The Database Models for our application. 
         Includes models for ..., and .... Also includes a
         PaginatedAPIMixin from github.com/miguelgrinberg

IMPORTANT NOTE:
The code below includes all the code required for use of indexes, however the
actual code applying these indexes and querying them are commented out, because
the use of indexes in DynamoDB costs money. They have been left to demonstrate
implementation using indexes, if future developers have the funding for 
indexing.

'''
from flask import jsonify
from pynamodb.models import Model
from pynamodb.attributes import (UnicodeAttribute, BooleanAttribute,
                                 NumberAttribute, ListAttribute, MapAttribute)
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from copy import deepcopy

class MetricMap(MapAttribute):
    metric = NumberAttribute(default=-1.0)
    metric_name = UnicodeAttribute()
    metric_history = NumberAttribute(default=0)

class WordMap(MapAttribute):
    word = UnicodeAttribute(default='test')
    new_file_value = NumberAttribute(default=0)
    old_file_value = NumberAttribute(default=0)

class FileMap(MapAttribute):
    filename = UnicodeAttribute()
    language = UnicodeAttribute(default='Not Yet Processed')
    text = UnicodeAttribute(default='Not Yet Processed')
    metrics = ListAttribute(of=MetricMap, default=[])
    sentences = ListAttribute(of=WordMap, default=[])


class SubjectIdIndex(GlobalSecondaryIndex):
    """
    This class represents a global secondary index for submission_id, an
    attribute of Submissions.
    """
    class Meta:
        read_capacity_units = 100
        write_capacity_units = 5
        projection = AllProjection()        # All attributes are projected

    # The index's hash key, must also exist in the model
    subject_id = UnicodeAttribute(hash_key=True)

class WorkshopIdIndex(GlobalSecondaryIndex):
    """
    This class represents a global secondary index for workshop_id, an
    attribute of Submissions.
    """
    class Meta:
        read_capacity_units = 100
        write_capacity_units = 5
        projection = AllProjection()        # All attributes are projected

    # The index's hash key, must also exist in the model
    workshop_id = UnicodeAttribute(hash_key=True)

class AssignmentIdIndex(GlobalSecondaryIndex):
    """
    This class represents a global secondary index for subject_id, an
    attribute of Submissions.
    """
    class Meta:
        read_capacity_units = 100
        write_capacity_units = 5
        projection = AllProjection()        # All attributes are projected

    # The index's hash key, must also exist in the model
    assignment_id = UnicodeAttribute(hash_key=True)


class Submissions(Model):
    class Meta:
        table_name = "submissions"
        # Specifies the write & read capacities
        write_capacity_units = 1
        read_capacity_units = 10
    submission_id = UnicodeAttribute(hash_key=True)
    student_id = UnicodeAttribute(range_key=True)
    validated = BooleanAttribute(default=False)
    files = ListAttribute(of=FileMap)

    # Index attributes
    subject_id = UnicodeAttribute()
    workshop_id = UnicodeAttribute()
    assignment_id = UnicodeAttribute()

    # Indexes
    # subject_id_index = SubjectIdIndex()
    # workshop_id_index = WorkshopIdIndex()
    # assignment_id_index = AssignmentIdIndex()


    @staticmethod
    def get_submission(submission_id):
        """
        Retrieve the Submission object from the DB for a specified submission.
        Should only be called after the submission_id is validated.

        :param submission_id:
        :return: A submission object with the given submission_id.
        """

        try:
            result = next(Submissions.query(submission_id))
        except StopIteration:
            return None

        return result

    @staticmethod
    def get_validated_subs_by_student(student_id):
        """
        Retrieve a list of Submission objects as from the DB for a specified 
        student.
        :param student_id:
        :return: List of the submissions
        """
        return [s for s in Submissions.scan(
                Submissions.student_id.contains(student_id) & 
                Submissions.validated.__eq__(True)
                )]

    @staticmethod
    def get_submissions_by_subject(subject_id):
        """
        Retrieve all Submissions from the database related to a subject.
        :param subject_id:
        :return: A list of submission objects with the given subject_id.
        """
        return [s.to_dict() 
                for s in Submissions.scan(Submissions.subject_id.contains(
                    subject_id))]
        # return [s.to_dict() 
        #         for s in Submissions.subject_id_index.query(subject_id)]

    @staticmethod
    def get_submissions_by_workshop(workshop_id):
        """
        Retrieve all Submissions from the database related to a workshop.
        :param workshop_id:
        :return: A list of submission objects with the given workshop_id.
        """
        return [s.to_dict() 
                for s in Submissions.scan(Submissions.workshop_id.contains(
                    workshop_id))]
        # return [s.to_dict() 
        #         for s in Submissions.workshop_id_index.query(workshop_id)]

    @staticmethod
    def get_submissions_by_assignment(assignment_id):
        """
        Retrieve all Submissions from the database related to an assignment.
        :param assignment_id:
        :return: A list of submission objects with the given assignmenty_id.
        """
        return [s.to_dict() 
                for s in Submissions.scan(Submissions.assignment_id.contains(
                    assignment_id))]
        # return [s.to_dict()
        #         for s in Submissions.assignment_id_index.query(assignment_id)]


    def to_dict(self, complete=False):
        vals = deepcopy(self.attribute_values)
        files = []
        for filemap in vals['files']:
            fm = filemap.attribute_values
            fm['text'] = fm['text'] if complete else fm['text'][:20] + "..."
            fm['metrics'] = [m.attribute_values for m in fm['metrics']]
            fm['sentences'] = [sentence.attribute_values for sentence in fm['sentences']]
            files.append(fm)
        vals['files'] = files
        return vals

