# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

FROM python:3.7.4-slim-buster

# COPY Necessary documents and folders
WORKDIR "/app"
COPY . .

# Note: aws.config file must be created by gitlabci job
# Please refer to gitlab-ci.yml for more information
# It is needed to pass configuration variables from Gitlab to
# this docker file on build time
# To test without pipeline, create aws.config file in same folder
# with following entries:
# AWS_ACCESS_KEY_ID=[Value]
# AWS_SECRET_ACCESS_KEY=[Value]
# AWS_REGION=[Value]

# Set default shell as Bash. Needed for 'source' command
SHELL ["/bin/bash", "-c"]

# Install python dependencies
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile

# Configure aws. This is need to acces AWS services
RUN source aws.config; aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID \
&& aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY \
&& aws configure set default.region $AWS_REGION

# This prints the above configuration. Proves that command passed
RUN aws configure list

# Removes created file that contains sensitive information
RUN rm aws.config

# No Entry Point was added to leave more freedom to docker-compose

# TODO check with Wenqiang the following is needed
#ENV LC_ALL C.UTF-8
#ENV LANG C.UTF-8