# teamic-backend-algo

A Micro-service that processes student submissions using Stylometric algorithms to verify authorship, storing the results.

# Deploy Algo Service on AWS Server

1. Firstly you need to browse DevOps repository and set up DevOps environment on AWS server.

2. Create a new repository on Gitlab. Upload the source code to this repository.

3. The pipeline of Gitlab should automatically run, but it will meet the problem at this step. We need firstly set the environment variable.

4. Set environment variable:

   1. Go to home page of the gitlab repository

   2. Go to Settings → CI/CD

   3. Find Variables and click Expand. You need fill in environment variables here.

   4. The following are the variables

   5. | APP_HOST              | 0.0.0.0                                                      |                                          |                                  |
      | --------------------- | ------------------------------------------------------------ | ---------------------------------------- | -------------------------------- |
      | APP_PORT              | 8000                                                         |                                          |                                  |
      | AWS_ACCESS_KEY_ID     | <[AWS key id](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | AKIA5RJHJOKXKJQJT3MD                     |                                  |
      | AWS_DB_HOST           | http://dynamodb.ap-southeast-2.amazonaws.com/                |                                          |                                  |
      | AWS_REGION            | ap-southeast-2                                               |                                          |                                  |
      | AWS_S3_BUCKET         | <[AWS bucket name](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | teamic-s3-bucket-cosmicfish              |                                  |
      | AWS_SECRET_ACCESS_KEY | <[AWS secret key](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | jzoHeWngStp2ayTwEEqqcK0IYFiQWHJdRAjLC1FF |                                  |
      | DEPLOY_HOST           | <[AWS service server](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | 54.66.92.162                             |                                  |
      | PRIVATE_SSH_KEY       | <[AWS ssh private key](https://confluence.cis.unimelb.edu.au:8443/display/COMP900822020SM2AIKoala/Set+up+AWS+Services)> | -----BEGIN RSA PRIVATE KEY-....          | This value should be set to file |

5. Run the pipeline again. This time it will success and build the algo service in AWS server.

6. Sometimes the pipeline will fail and show you should set up the ic_network. Just ssh to the server and run the command line manually.

   `````
   docker network create --driver bridge ic_network || true
   `````

7. Ssh to the server to check if all dockers are running. There should be four dockers, which are "algo_algo_1" "algo_redis_worker_1" "algo_redis_1" "webapi_app_1".

8. If Redis docker are not running, you should do these commands.

   ```
   cd /algo
   sudo chown -R 1001:1001 ./redis
   docker
   docker-compose up
   ```

9. Check if Algo service is normally running now.

# Deploy Algo Service locally

NOTE: Local installation methods are legacy methods left by previous teams. We strongly recommend that you use pipeline to install project in AWS server instead.

## Development Setup

### Requirements
- __Install [autoenv](https://github.com/inishchith/autoenv)__: Install it and then, because we use `.aenv` (due to a temporary docker-compose issue) open up ~/.autoenv/activate.sh (or wherever this is on your Windows machine) in your text editor and change line 2, where it specifies the autoenv env filename, to `.aenv`. (Explanation below)
- Install [Python 3.7](https://www.python.org/downloads/release/python-374/).

### Steps
1. __Install Virtual Environment and dependencies__: Simply run `make env` to install the virtual environment with python 3.7, activate it and all of the dependencies. If this is, for some reason, not workingm, try the alternative steps below.
2. __Activate the environment__: `source venv/bin/activate`
3. __Install & Configure AWS__: Refer to **AWS CLI Configuration** section
4. __Configure Local AWS Services__: For local development, either configure Localstack or DynamoDB-local (explained below). This may require changes in `config_app.py`.
5. __Run the services in one tab__: `make services`  (If Could not create server TCP listening socket *:6379: bind: Address already in use occurs, use "ps aux | grep redis" then "kill -9 ENTER_PORT")
6. __Create an local S3 Bucket with the proper [ACL](https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html)__: `python s3.py` (if that fails, `aws --endpoint-url=http://localhost:4572 s3api put-bucket-acl --bucket local-bucket --acl public-read`)
7. __Run the redis worker in another__: `make worker`
8. __Run the app__: `make server` (If 8000 is occupied, it can be killed by "sudo lsof -t -i tcp:8000 | xargs kill -9")

### Alternative Steps
1. **Create a Virtual Environment**. E.g. `python3.7 -m venv venv`
2. __Activate the environment__: `source venv/bin/activate`
3. **Install Pipenv**: Use `pip` to install Pipenv and its dependencies
`pip install pipenv`
4. **Install Pipenv Dependencies**: `pipenv install`
5. __Configure AWS__: Refer to **AWS CLI Configuration** section
6. __Configure Local AWS Services__: Either configure Localstack or DynamoDB-local (explained below). This may require changes in `config_app.py`.
7. __Run the services in one tab__: `docker-compose up -d && redis-server`  (If Could not create server TCP listening socket *:6379: bind: Address already in use occurs, use "ps aux | grep redis" then "kill -9 ENTER_PROCESS ID")
8. __Create an local S3 Bucket with the proper [ACL](https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html)__: `python s3.py` (if that fails, `aws --endpoint-url=http://localhost:4572 s3api put-bucket-acl --bucket local-bucket --acl public-read`)
9. __Run the redis worker in another__: `rq worker ic-api-tasks`
10. __Run the app__: `gunicorn manage:app -b localhost:8000` (If 8000 is occupied, it can be killed by "sudo lsof -t -i tcp:8000 | xargs kill -9")

__NOTES__
1. __Autoenv Issue__: this is a problem because the Docker Compose developers were super thoughtless and decided to enforce `.env` files to conform to their format, or else the docker service you're trying to run implodes. This will be rectified soon in an update (a commit fixing this was pushed on the 14th of Aug) but it is still not live.
2. If `pipenv install` isn't installing into your virtual environment, you can try one of the following: 
    + `pipenv install --python=python`; or 
    + `pipenv install --python=./python`; or 
    + `python3.7 -m pipenv install`;


### AWS CLI Configuration
To access AWS S3 and DynamoDB services, you need to obtain credentials (Access key and secret).
This can be obtained by using IAM from AWS console. It is, however, necessary to
configure aws cli even if no real key is provided for development purposes. Without
it, the used aws libraries (boto3 for S3 and pynamodb for Dynamodb) will fail.
When in development mode, you can provide dummy non real information. However,
it is better to provide real credentials from the start so you can run the system
on production environment on localhost for testing purposes. <br>
To configure aws cli, run the following command:
```
aws configure
```
You'll be asked to provide:
1. **aws_access_key_id:** Provide real or dummy key
2. **aws_secret_access_key:** Provide real or dummy ,ey
3. **default_region_name:** Should be one of AWS regions. The system was built
                            in Australia, so default region was 'ap-southeast-2'
4. **output_format:**  Should be blank, just press enter.


### DynamoDB-local
Simply run: `docker run -p 7000:8000 amazon/dynamodb-local`

### Localstack
1. __Start local AWS Services__: `docker-compose up -d`
2. __Create an S3 Bucket with the proper [ACL](https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html)__: `python s3.py` (if that fails, `aws --endpoint-url=http://localhost:4572 s3api put-bucket-acl --bucket local-bucket --acl public-read`)

__Local Shell__: Access the shell at `http://localhost:4564/shell/`.

Examples:
_List existing tables_
```javascript
// Check for existing tables
 var params = {
 };
 dynamodb.listTables(params, function(err, data) {
   if (err) console.log(err, err.stack); // an error occurred
   else     console.log(data);           // successful response
});
```

_Adding Table Entries_
```javascript
var params = {
    TableName: 'sessions',
    Item: { // a map of attribute name to AttributeValue
        submission_id: "sub1",
        student_id: "st1",
        fname: "known01.pdf",
        subject_id: "sjt1",
        workshop_id: "ws1",
        assignment_id: "as1",
        text: "None",
        metric: -1.0,
        validated: false
    }
};
dynamodb.putItem(params, function(err, data) {
    if (err) console.log(err); // an error occurred
    else console.log(data); // successful response
});
```

_Scanning Existing Table_
```javascript
var params = {
    TableName: 'submissions',
};
dynamodb.scan(params, function(err, data) {
    if (err) print(err); // an error occurred
    else print(data); // successful response
});
```

# Running Algo Microservice

## Running Individual Python Files
To run, for example, the `processor.py` file by itself, simply use the command `python -m app.algo.processor` with the virtual environment activated.

## Running in Development mode
Running the service in development means the service depends on local services
to run code. This is helpful for development and testing purposes as well. The
steps explained previous (in Steps and alternative steps section) should run
the application in development mode by default. This also depends on two services
run by a docker-compose file, which starts a local versions of S3 and Dynamodb
used for development and testing purposes.

## Running in Production mode(In localhost)
To run the service in production mode, several configurations need to take place
in order for the system start in *aws* mode. This will run the microservice and
will link it to real AWS S3 and DynamoDB. This is helpful to test how the deployed
system will react. <br>
The following variables are needed to be included in the environment to run
in production mode:
```
FLASK_CONFIG=aws
AWS_DB_HOST=[......]
AWS_REGION=[......]
AWS_S3_BUCKET=[......]
```
Use export FLASK_CONFIG="aws"
    export AWS_DB_HOST="......"
    export AWS_REGION="......"
    export AWS_S3_BUCKET="......"
Where:
* **FLASK_CONFIG:** Must always equal to 'aws' for the configuration to switch
to aws production configurations.
* **AWS_DB_HOST:** Refers to Real Dynamodb host
* **AWS_S3_BUCKET:** Refers to Real S3 Bucket used to save files
**Cautions:** Before running the system on production mode, make sure that aws
cli is configured with real credentials. Refer to **AWS CLI Configuration**
for more information.

After that run "gunicorn manage:app -b localhost:8000".

## Running in Testing mode
To run automated unit test, make sure to run needed services before (# 4 & 5 in
Steps section). This is important for the tests to run smoothly. In addition,
the following environment variable must be included:
```
AWS_DB_HOST=http://localhost:4569
```
This assumes that localstack is being used as local DynamoDB. If not, change 
accordingly. <br>
To run unit test:
```
pytest
```




