# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# This script is used to test that ansible playbook 'site.yml' is
# functioning as intended

# For this script to work, several files must be added. Note that these files
# are not included in the git repo as they are created as part of the gitlabci
# deploy job. This script should be used ONLY for testing the ansible script

# Before Running the script, do the following files:
# 1. Copy the needed private key to deploy folder.
# 2. Create an empty file named 'inventory'
# 3. Add the following line
#    algo ansible_host=[DEPLOY_HOST] ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=[PRIVATE_SSH_KEY]
#    where [DEPLOY_HOST] should be replaced by target ip address
#    and [PRIVATE_SSH_KEY] is the name of the private key copied in step (1)
# 4. Create an empty file named 'variables.yml'. This file will include
#    needed info to login to gitlab registry to retrieve build images
# 5. Add the following three lines to the file :
#    CI_REGISTRY_USER: [REGISTRY_USER]
#    CI_REGISTRY_PASSWORD: [REGISTRY_PASSWORD]
#    CI_REGISTRY: [REGISTRY]
#    Where this variables are credentials for gitlab registry. The CI_REGISTRY
#    is expected to be 'registry.gitlab.com', but can be changed if the built
#    image is hosted elsewhere 'e.g: docker hub'
# 6. Create an empty file named '.env'. This file is read by docker-compose
#    for needed environment variables.
# 7. Add needed variables to the file. Refer to docker-compose.yml file for
#    more information regarding variables to include
#    E.g: APP_HOST=localhost

# Note: The docker-compose expects a built image. Make sure the image exists
# in the registry the image will be fetched from

# You need ansible version 2.8 to run this script

export ANSIBLE_HOST_KEY_CHECKING=FALSE
ansible-playbook -i inventory site.yml