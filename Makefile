.PHONY: docs test

help:
	@echo "  env                create a development environment using virtualenv"
	@echo "  server             run the app on a gunicorn server"
	@echo "  clean              remove unwanted files like .pyc's"
	@echo "  db                 update the database after updating the models"
	@echo "  reset_local_db     reset and re-initialise the local database"

	# @TODO: REPLACE w/ appropriate new commands
	# @echo "  reset_heroku_db    reset and upgrade the Heroku database (using migrations committed)"
	# @echo "  rebuild_heroku_db  reset and re-initialise the Heroku database. Requires Heroku CLI & app installed"
	@echo "  test               run tests using py.test"
	@echo "  test_cov           run tests using py.test, generating test coverage report. The report is then found by clicking ta-bilby_backend/htmlcov/index.html"
	@echo "  lint               check style with flake8"
	@echo "  gitlab-build		Runs gitlab-runner pipeline locally. Please install gitlab-runne for this command to execute"

env:
	python3.7 -m venv venv && \
	source venv/bin/activate && \
	pip install pipenv && \
	pipenv install

server:
	exec venv/bin/gunicorn manage:app

services:
	docker-compose up -d && \
	redis-server

worker:
	rq worker ic-api-tasks

clean:
	python manage.py clean

db:
	python manage.py db migrate && \
	python manage.py db upgrade

reset_local_db:
	rm -rf migrations || true && \
	rm 'data-dev.sqlite' || true && \
	python manage.py db init && \
	python manage.py db migrate && \
	python manage.py db upgrade && \
	flask add_users

# @TODO: TO BE REPLACED by methods for AWS or whatever hosting solution we use
# reset_heroku_db:
#     heroku pg:reset DATABASE -a ta-data-api --confirm ta-data-api && \
#     heroku run flask db upgrade -a ta-data-api && \
#     heroku run flask add_users -a ta-data-api

# rebuild_heroku_db:
#     heroku pg:reset DATABASE -a ta-data-api --confirm ta-data-api && \
#     heroku run flask create_db -a ta-data-api && \
#     heroku run flask add_users -a ta-data-api

test:
	echo "Just running pytest tests, halting on first failure"
	python -m pytest tests --disable-pytest-warnings -x

test_cov:
	echo "Running py.test tests and generating coverage html docs."
	python -m pytest --cov=app --cov=config_app --cov-report=html --cov-report=term --cov-report=annotate tests --disable-pytest-warnings
	find . | grep -E "(__pycache__|\,cover|\.pyc$))" | xargs rm -rf

lint:
	exec venv/bin/flake8 --exclude=venv .

gitlab-build:
	gitlab-runner exec docker --docker-privileged --docker-pull-policy if-not-present --docker-cache-dir cache build

gitlab-test:
	gitlab-runner exec docker --docker-privileged --docker-pull-policy never --docker-cache-dir 'cache' --docker-volumes '/cache' --docker-volumes '/etc/hosts:/etc/hosts' test_app