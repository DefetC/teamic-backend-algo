#!/bin/bash

# Used to install python libraries and simultaneously update the pipenv files
library="$1"
if [ -z "$1" ]
  then
    echo "No argument supplied, installing libraries in requirements.txt"
    pipenv install 
  else
    pipenv install "$library"
    pipenv lock
fi
