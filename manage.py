#!/usr/bin/env python
'''
Name: manage.py
Author: David Stern - Integrity Checking Project
Re-use: Uses some code from github.com/miguelgrinberg/flasky and other sources,
        where stated.

# @TODO: Replace these
# To reset heroku database
make reset_heroku_db

# Or if no migrations folder:
make rebuild_heroku_db

# To reset database locally
make reset_local_db

# To migrate and upgrade database locally, after changing models.py
make db
'''

import os
# from dotenv import load_dotenv

# Used to load environment variables into the namespace
# dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
# if os.path.exists(dotenv_path):
#     load_dotenv(dotenv_path)

# For scripting and providing commands
from flask_script.commands import Clean
from flask_script import Manager
import sys

# Our application
from app import create_app
from app.models import Submissions


# Initialise our application.
app = create_app(os.getenv('FLASK_CONFIG') or 'default')

# Used to add Flask-Script scripts
# from flask_migrate import Migrate, upgrade, MigrateCommand
manager = Manager(app)
# migrate = Migrate(app, db)
# manager.add_command('db', MigrateCommand)
manager.add_command("clean", Clean())


@app.shell_context_processor
def make_shell_context():
    '''Creates a python REPL with several default imports in the context of 
       the app.
    '''
    # return dict(db=db, Submissions=Submissions)#, Submission=Submission)
    return dict(Submissions=Submissions)#, Submission=Submission)
    # return dict(db=db, User=User, Session=Session, Lap=Lap, 
    #             Trackpoint=Trackpoint, SessionData=SessionData)


# @app.cli.command('delete_rows')
# def delete_rows():
#     '''Delete all rows of all tables from the database.
#     Run using: `flask delete_rows` or on Heroku,
#                `heroku run 'flask delete_rows' -a ta-data-api`
#     Ensure FLASK_APP=manage.py is set
#     '''
#     models = [Trackpoint, SessionData, Session, User]
#     for model in models:
#         db.session.query(model).delete()
#         db.session.commit()



# @app.cli.command('create_db')
# def create_db():
#     '''Creates a db with all of the tables defined in the SQLAlchemy models.
#     '''
#     db.create_all()


# @manager.command
# def deploy():
#     '''Run deployment tasks.
#     Use: `python manage.py deploy` or 
#          `heroku run 'python manage.py deploy' -a ta-data-api`
#     '''
#     # migrate database to latest revision
#     upgrade()


# @manager.command
# def downgrade():
#     '''Reverse a previous database migration upgrade.
#     '''
#     # reverse most recent database migration
#     downgrade()


if __name__ == "__main__":
    manager.run()
