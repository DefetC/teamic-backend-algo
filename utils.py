import pandas as pd

# create a dataframe from a word matrix
def wm2df(wm, feat_names):
    '''From: https://towardsdatascience.com/hacking-scikit-learns-vectorizers-9ef26a7170af'''
    
    # create an index for each row
    doc_names = ['Doc{:d}'.format(idx) for idx, _ in enumerate(wm)]
    df = pd.DataFrame(data=wm.toarray(), index=doc_names,
                      columns=feat_names)
    return(df)
