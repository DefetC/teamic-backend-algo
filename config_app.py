"""
/config_app.py
Author: David Stern, but uses code from @miguelgrinberg:
https://github.com/miguelgrinberg/flasky

Purpose: Uses the Configuration Class Pattern to set alternative environment
         variables & defines different behaviours for different application
         configurations.
         Used by the application factory in /app/__init__.py.
"""

import tempfile
import os
from app.models import Submissions
from settings import settings
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """
    Base configuration. All configurations import this configuration.
    """

    # Security
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SSL_REDIRECT = False

    # Redis Worker
    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'

    # Algorithm Settings
    ALGO_SETTINGS = settings

    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    """Configuration used in all production settings.
    Currently only AWS production is used, but others may be created.
    """

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        # Redis Worker
        REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'


class AWSConfig(ProductionConfig):
    """
    Production Configuration used specifically on AWS.
    """

    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # Configures AWS S3
        S3_BUCKET_NAME = os.getenv('AWS_S3_BUCKET')

        # Configures AWS DynamoDB
        # No need to configure credentials. It should be
        # configured through aws cli on build
        AWS_DB_HOST = os.getenv('AWS_DB_HOST')
        AWS_REGION = os.getenv('AWS_REGION')
        Submissions.Meta.host = AWS_DB_HOST
        Submissions.Meta.region = AWS_REGION

        # Print Information
        # TODO Use logging instead
        print("AWS Configurations:")
        print(f'Using S3 Bucket: {S3_BUCKET_NAME}')
        print(f'Using DynamoDB with Host: {AWS_DB_HOST}')
        print(f'Using DynamoDB with Region: {AWS_REGION}')

        print("check print")

        # # handle reverse proxy server headers
        # from werkzeug.contrib.fixers import ProxyFix
        # app.wsgi_app = ProxyFix(app.wsgi_app)
        #
        # # log to stderr
        # import logging
        # import sys
        # from logging import StreamHandler
        # file_handler = StreamHandler(sys.stdout)
        # file_handler.setLevel(logging.INFO)
        # app.logger.addHandler(file_handler)


class DevelopmentConfig(Config):
    """
    Development server/sandbox. Where unit testing is performed by the dev.
    """
    DEVELOPMENT = True
    DEBUG = True
    CACHE_TYPE = 'null'

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        # Specifies S3 bucket used for local development
        S3_HOST = "http://localhost:4572"  # With localstack
        S3_BUCKET_NAME = "local-bucket"

        # Specifies the DynamoDB region, host defaults
        Submissions.Meta.region = "ap-southeast-2"
        Submissions.Meta.host = "http://localhost:4569"  # For localstack
        # Submissions.Meta.host = "http://localhost:7000"   # For DynamoDB-local

        # Creates Table if non existent
        if not Submissions.exists():
            print("Creating table...")
            Submissions.create_table(wait=True)


class TestingConfig(Config):
    """
    Used for testing the application.
    """

    # Test Settings
    TESTING = True
    WTF_CSRF_ENABLED = False

    # No Cache
    CACHE_TYPE = 'null'

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        # Specifies the DynamoDB region, host defaults
        Submissions.Meta.region = "ap-southeast-2"
        # Submissions.Meta.host = "http://localhost:7000"  # For DynamoDB-local
        # Submissions.Meta.host = "http://localhost:4569"  # For localstack
        # For Testing Deployment
        Submissions.Meta.host = os.getenv('AWS_DB_HOST') \
                                or "http://dynamodb:8000"

        # Specifies another table name to avoid accidents
        Submissions.Meta.table_name = "submissions_test"


# Used to refer to different configurations succinctly.
config = {
    'base': Config,
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'aws': AWSConfig,
    'default': DevelopmentConfig
}
