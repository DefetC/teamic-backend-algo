# University of Melbourne
# School of Engineering
# SWEN90013 Masters Advanced Software Project - 2019
# Team IC

# Test Models and interaction with database
import pytest
from pynamodb.pagination import ResultIterator

from app import create_app
from app.models import Submissions, MetricMap, FileMap


class TestModel:

    @staticmethod
    def sample_submission():
        sub: Submissions = Submissions()
        sub.submission_id = '4f4ab9e6-f00d-11e9-95da-acde48001122'
        sub.student_id = '666666'
        sub.validated = False
        metric = MetricMap()
        metric.metric = 0.9
        metric.metric_name = 'Cosine'
        metric.metric_history = 0
        file = FileMap()
        file.filename = 'assignment404.txt'
        file.text = 'Example'
        file.metrics = [metric]
        sub.files = [file]
        sub.subject_id = '4949'
        sub.workshop_id = '4453'
        sub.assignment_id = '33562'

        return sub

    @classmethod
    def setup_class(cls):
        # Initialise app with testing settings
        create_app('testing')

        # Create fresh table
        if Submissions.exists():
            Submissions.delete_table()
        Submissions.create_table(read_capacity_units=1, write_capacity_units=1,
                                 wait=True)

    @classmethod
    def teardown_class(cls):
        Submissions.delete_table()

    def test_saved(self):
        """
        Test a submission is stored into database
        :return:
        """
        sub = self.sample_submission()
        sub.save()

        # Test submission was persisted
        assert Submissions.count() == 1

    def test_retrieved(self):
        """
        Test submission can be retrieved by submission id
        """
        sub = self.sample_submission()
        result = Submissions.get_submission(sub.submission_id)
        assert result.student_id == 1
    #
    # def test_retrieve_by_subject(self):
    #     """
    #     Test submission can be retrieved by subject, and that
    #     only verified submissions are retrieved
    #     """
    #     sub = self.sample_submission()
    #     result: ResultIterator = Submissions.get_submissions_by_subject(
    #         sub.subject_id)
    #     result = list(result)
    #     assert len(result) == 1
    #     assert result[0].fname == sub.fname
    #
    # def test_retrieve_by_workshop(self):
    #     """
    #     Test submission can be retrieved by workshop, and that
    #     only verified submissions are retrieved
    #     """
    #     sub = self.sample_submission()
    #     result = Submissions.get_submissions_by_workshop(
    #         sub.workshop_id)
    #
    #     result = list(result)
    #     assert len(result) == 1
    #     assert result[0].fname == sub.fname
    #
    # def test_retrieved_when_validated(self):
    #     """
    #     Test submission can be retrieved by subject, when a previously
    #     non validated score is validated now
    #     """
    #     sub = self.sample_submission()
    #     sub2 = self.sample_submission2()
    #     result = Submissions.get_submission(
    #         sub2.submission_id)
    #     new_sub2 = result.next()
    #     new_sub2.update(actions=[Submissions.validated.set(True)])
    #
    #     result = Submissions.get_submissions_by_subject(sub2.subject_id)
    #     result = list(result)
    #
    #     assert len(result) == 2
    #     assert result[1].submission_id == sub.submission_id
    #     assert result[0].submission_id == sub2.submission_id
